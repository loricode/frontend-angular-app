import { Component, OnInit } from '@angular/core';
import { AppService } from '../services/app.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  listApps:[] = [];
  tipos:[] = []
  id = '';

  appsForm = new FormGroup({
    titulo: new FormControl(''),
    descripcion: new FormControl(''),
    version: new FormControl(''),
    tipo:new FormControl('')
  });
  
  constructor(public appService:AppService) { }

   ngOnInit(): void {
     this.getApps();
    }
  
    public getApps(): void {
      this.appService.getApps().subscribe(response => {   
        const { apps, tipos } = response;
        console.log(response)
        this.listApps = apps; 
        this.tipos = tipos
      });
    }


   public addApps(): void { 
      this.appService.addApps(this.appsForm.value).subscribe(() => {
        this.getApps();
        this.appsForm.reset('');
      })
    }

    public deleteApps(id: string): void {
      if(window.confirm("¿Estas seguro de querer eliminar?")){
         this.appService.deleteApps(id).subscribe(() => {   
           this.getApps();
         },(error) => {
           console.error(error);
       })
      }        
    }


  public getApp(id: string): void {
    this.appService.getApp(id).subscribe(response => {  
       const { id, appDESCRIPCION, appTITULO, appVERSION, tipos_apps_id } = response; 
       this.id = id;  
       this.appsForm.setValue({
        titulo:appTITULO,
        descripcion:appDESCRIPCION,
        version:appVERSION,
        tipo:tipos_apps_id
      });
      },(error) => {
         console.error(error);
    })
  }

  updateApps():void {
    const obj = this.appsForm.value;
    let id = this.id;
    this.appService.updateApps(obj, id).subscribe(() => { 
      this.getApps();    
      this.appsForm.reset('');
     },(error) => {
        console.error(error);
    })
  }
    
}
