import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AnunciosComponent } from './anuncios/anuncios.component';

const routes: Routes = [
  { path:'',  component:HomeComponent, pathMatch: 'full' },
  { path:'home',  component:HomeComponent},
  { path:'anuncio', component:AnunciosComponent},
  { path:'anuncio/:id', component:AnunciosComponent},
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
