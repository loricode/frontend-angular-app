import { Injectable } from '@angular/core';
import { environment }  from '../../environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  baseUrl = environment.baseUlr;

  constructor(public http:HttpClient) { }

  public getApps() {
    return this.http.get<any>(this.baseUrl+'/apps');
   }

   public addApps(obj: any) {
    return this.http.post<any>(this.baseUrl+'/apps', obj)
  }

  public deleteApps(id: string) {
    return this.http.delete<any>(this.baseUrl+`/apps/${id}`);
  }

  public getApp(id: string) {
    return this.http.get<any>(this.baseUrl+`/apps/${id}`);
  }
  
  public updateApps(obj: any, id:string) {
    return this.http.put<any>(this.baseUrl+`/apps/${id}`, obj)
  }

  
  public addAnuncio(obj: any) {
    return this.http.post<any>(this.baseUrl+'/apps/anuncios', obj)
  }

}
