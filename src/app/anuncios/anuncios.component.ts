import { Component, OnInit } from '@angular/core';
import { AppService } from '../services/app.service';
import { FormGroup, FormControl } from '@angular/forms'

@Component({
  selector: 'app-anuncios',
  templateUrl: './anuncios.component.html',
  styleUrls: ['./anuncios.component.css']
})
export class AnunciosComponent implements OnInit {

  anuncioForm = new FormGroup({
    titulo: new FormControl(''),
    descripcion: new FormControl(''),
    imagen: new FormControl(''),
  });
  constructor(public appService:AppService) { }

  ngOnInit(): void {
  }

  public addAnuncio(): void { 
    this.appService.addAnuncio(this.anuncioForm.value).subscribe((response) => { 
      console.log(response)
      this.anuncioForm.reset('');
    })
  }


}
